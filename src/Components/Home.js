import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  render () {
    return (
      <div className="jumbotron text-center p-3 mb-2 bg-light text-dark">
        <div className="container">
          <h1 className="jumbotron-heading">Winfooz assignment</h1>
          <blockquote className="blockquote text-muted mb-5 d-block">
            <p className="mb-0">Smile in the mirror. Do that every morning and you'll start to see a big difference in your life. </p>
            <footer className="blockquote-footer">Not Hasan Zahran</footer>
          </blockquote>

          <Link to="/PopulationCounts" className="btn btn-primary m-2 btn-block btn-info my-3 font-weight-bold">Population Counts</Link>
          <Link to="/SqaureRoot" className="btn btn-primary m-2 btn-block btn-info font-weight-bold">Sqaure Root</Link>
        </div>
      </div>
    );
  }
}

export default Home