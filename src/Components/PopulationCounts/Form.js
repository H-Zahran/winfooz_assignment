import React from 'react';

const CalculateYears = ( props ) => {
  return (
    <div className="card mb-2">
      <div className="card-header">
        Population counts
      </div>
      <div className="card-body">
        <form className="needs-validation">
          <div className="form-group">
            <label>Current population</label>
            <input
              name="k0"
              type="text"
              className={`form-control ${props.data.errork0 ? 'is-invalid' : ''}`}
              placeholder='Current'
              pattern="[0-9]+(\.[0-9]{0,2})?%?"
              title="Number value"
              onChange={props.handleChange}
            />
            <div className="invalid-feedback">
              {props.data.errork0}
            </div>
          </div>
          <div className="form-group">
            <label>Increase (Percentage)</label>
            <input
              name="increase"
              type="text"
              className={`form-control ${props.data.errorincrease ? 'is-invalid' : ''}`}
              placeholder='Increase'
              pattern="[0-9]+(\.[0-9]{0,2})?%?"
              title="Number value"
              onChange={props.handleChange}
            />
            <div className="invalid-feedback">
              {props.data.errorincrease}
            </div>
          </div>
          <div className="form-group">
            <label>Extra</label>
            <input
              name="extra"
              type="text"
              className={`form-control ${props.data.errorextra ? 'is-invalid' : ''}`}
              placeholder='Extra'
              pattern="[0-9]+(\.[0-9]{0,2})?%?"
              title="Number value"
              onChange={props.handleChange}
            />
            <div className="invalid-feedback">
              {props.data.errorextra}
            </div>
          </div>
          <div className="form-group">
            <label>Target</label>
            <input
              name="t"
              type="text"
              className={`form-control ${props.data.errort ? 'is-invalid' : ''}`}
              placeholder='Target'
              pattern="[0-9]+(\.[0-9]{0,2})?%?"
              title="Number value"
              onChange={props.handleChange}
            />
            <div className="invalid-feedback">
              {props.data.errort}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default CalculateYears