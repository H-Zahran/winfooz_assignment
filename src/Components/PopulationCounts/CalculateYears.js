import React, { Component } from 'react';
import Form from './Form';

class CalculateYears extends Component {
  state = {
    years: 0,
    k0: 0,
    increase: 0,
    extra: 0,
    t: 0,
    errork0: '',
    errorincrease: '',
    errorextra: '',
    errort: '',
  }

  handleValueChange = event => {
    const target = event.target;
    if ( target.validity.valid ) {
      const value = Number( target.value );
      let targetErr = `error${target.name}`;
      let targetInput = target.name;
      this.setState( {
        [targetInput]: value,
        [targetErr]: ""
      }, () => this.numberOfYearsHandler() );
    }
    else {
      let targetErr = `error${target.name}`;
      this.setState( {
        [targetErr]: "Please enter a valid number"
      } );
    };
  }

  numberOfYearsHandler = () => {
    const k0 = Number( this.state.k0 );
    const increase = Number( this.state.increase );
    const extra = Number( this.state.extra );
    const t = Number( this.state.t );
    if ( k0 === t || k0 < 0 || increase < 0 || extra < 0 || t < 0 || ( ( increase / 100 ) + extra === 0 ) ) {
      return 0;
    } else {
      let years = 0;
      for ( let i = k0; i < t; i += ( i * ( increase / 100 ) + extra ) )
        years++;
      this.setState( { years: years } );
    }
  }
  render () {
    return (
      <div className="container">
        <div className="row">

          <div className="col-md-8">
            <Form
              data={this.state}
              handleChange={this.handleValueChange}
            />
          </div>

          <div className="col-md-4">
            <div className="card mb-2">
              <div className="card-header">
                Result
              </div>
              <div className="card-body">
                {this.state.years ? (
                  < h5 > {this.state.years} years</h5>
                ) : null}
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
export default CalculateYears