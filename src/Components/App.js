import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import logo from '../wonfooz.png';
import Home from './Home';
import CalculateYears from './PopulationCounts/CalculateYears';
import SqaureRoot from './SqaureRootCalculation/SqaureRoot';

class App extends Component {

  render () {
    return (
      <>
        <div className="d-block p-3 bg-dark text-center mb-5 shadow">
          <Link className="navbar-brand p-0 m-auto" to="/">
            <img src={logo} alt="Winfooz" />
          </Link>
        </div>

        <Route exact path="/" render={() => (
          <Home />
        )} />

        <Route path="/PopulationCounts" render={( { history } ) => (
          <CalculateYears /> )}
        />

        <Route path="/SqaureRoot" render={( { history } ) => (
          <SqaureRoot /> )}
        />

      </>
    );
  }
}
export default App