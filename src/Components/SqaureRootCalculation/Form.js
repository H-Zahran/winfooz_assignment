import React from 'react';

const CalculateSquareRoot = ( props ) => {
  return (
    <div className="card mb-2">
      <div className="card-header">
        Square Root
      </div>
      <div className="card-body">
        <form className="needs-validation">
          <div className="form-group">
            <label>Index</label>
            <input
              name="index"
              type="text"
              className='form-control'
              placeholder='Index'
              pattern="[0-9]+(\.[0-9]{0,2})?%?"
              title="Number value"
              onChange={props.sqaureRoot}
            />
          </div>
          <div className="form-group">
            <label>Radicand</label>
            <input
              name="radicand"
              type="text"
              className='form-control'
              placeholder='Radicand'
              pattern="[0-9]+(\.[0-9]{0,2})?%?"
              title="Number value"
              onChange={props.sqaureRoot}
            />
          </div>
        </form>
      </div>
    </div>
  );
}

export default CalculateSquareRoot