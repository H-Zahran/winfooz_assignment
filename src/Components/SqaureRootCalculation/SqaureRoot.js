import React, { Component } from 'react';
import Form from './Form';
import RootSymbol from '../../RootSymbol.svg';
// Math.pow( Math.abs( 9 ), 1 / 3 )
// Math.pow( n, 1 / root );
// Math.pow( 25, 1 / 2 ) == 5

class SqaureRoot extends Component {


  state = {
    root: 0,
    index: 0,
    radicand: 0,
  }

  sqaureRootHandler = event => {
    const target = event.target;
    const value = Number( target.value );
    let targetInput = target.name;
    this.setState( {
      [targetInput]: value,
    }, () => this.calculateHandler() );
  }


  calculateHandler = () => {
    console.log( this.state );
    let index = Number( this.state.index );
    let radicand = Number( this.state.radicand );
    let root = Math.pow( radicand, 1 / index );
    this.setState( { root } );
  }

  render () {
    return (
      <div className="container">
        <div className="row">

          <div className="col-md-8">
            <Form data={this.props} sqaureRoot={this.sqaureRootHandler} />
          </div>

          <div className="col-md-4">
            <div className="card mb-2">
              <div className="card-header">
                Result: {this.state.root}
              </div>
              <div className="card-body position-relative">
                <span className="index">{this.state.index}</span>
                <span className="radicand">{this.state.radicand}</span>
                <img className="img-fluid w-100" src={RootSymbol} alt="Squar Root Symbol" />
              </div>
            </div>
          </div>

        </div>
      </div>
    )
  }
}

export default SqaureRoot



